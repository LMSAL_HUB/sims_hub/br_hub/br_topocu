

from PyQt5 import Qt
from PyQt5 import QtGui
from PyQt5 import QtWidgets

import os
import sys
import math
import numpy as np

__all__ = ["tp_q","tp_connectivity","tp_cuda","tp_trace","tp_data","tp_integrate","tp_switch","tp_plot","tp_parse"]

from . import tp_parse
from . import tp_data
from . import tp_cuda
from . import tp_trace
from . import tp_q
from . import tp_connectivity
from . import tp_integrate
from . import tp_switch
from . import tp_plot


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    main = pt_plot.plotter(tp_switch.switch())
    main.setWindowTitle('Q Factor')
    main.show()

    sys.exit(app.exec_())
