
import numpy as np

from .tp_q import qCalculator, qCalculatornopars
from .tp_connectivity import connectivity, connectivitynopars
from .tp_integrate import integrate, integratenopars

class switch(qCalculator, connectivity, integrate):

    '''
    Class that switches between the program's different functions.
    '''

    def save(self):

        #Saves output to a .npy file in the current working directory

        if not os.path.exists(os.getcwd() + '/' + self.temp[0:5]):
            os.makedirs(os.getcwd() + '/' + self.temp[0:5])
        np.save(os.getcwd() + '/' + self.temp[0:5] + '/n_snap_' + str(self.snap), self.q)

    def __init__(self):
        super(switch, self).__init__()

        if self.opts.q:
            #Q-Factor
            self.trace_snapshot()
            self.init_q()

            for i in range (0, self.zlen):
                self.arrays = self.q_data[i]
                self.q[:,:,i] = self.calculate_q(self.plane if self.opts.slice else i)

            if(self.opts.save):
                self.save


        elif self.opts.connectivity:
            #Connectivity
            for i in range(0, self.zlen):
                self.init_trace()
                self.trace_plane(self.plane if self.opts.slice else i)
                self.init_connectivity()
                self.q[:,:,i] = self.connectivity(self.plane if self.opts.slice else i)

                if(self.opts.save):
                    self.save


        elif self.opts.integrate:
            #Integration of parallel electric field along field lines
            self.init_integrate()
            for i in range(0, self.zlen):

                self.integrate_plane(self.plane if self.opts.slice else i)
                self.q[:,:,i] = self.arrays['i']

            if(self.opts.save):
                self.save

class switchnopars(qCalculatornopars, connectivitynopars, integratenopars):

    '''
    Class that switches between the program's different functions.
    '''
    def save(self):

        #Saves output to a .npy file in the current working directory

        if not os.path.exists(os.getcwd() + '/' + self.temp[0:5]):
            os.makedirs(os.getcwd() + '/' + self.temp[0:5])
        np.save(os.getcwd() + '/' + self.temp[0:5] + '/n_snap_' + str(self.snap), self.q)

    def __init__(self,opts=None,parent=None):
        super(switchnopars, self).__init__(opts)

        if self.opts.q:
            #Q-Factor
            self.trace_snapshot()
            self.init_q()

            for i in range (0, self.zlen):
                self.arrays = self.q_data[i]
                self.q[:,:,i] = self.calculate_q(self.plane if self.opts.slice else i)

            if(self.opts.save):
                self.save


        elif self.opts.connectivity:
            #Connectivity
            for i in range(0, self.zlen):
                self.init_trace()
                self.trace_plane(self.plane if self.opts.slice else i)
                self.init_connectivity()
                self.q[:,:,i] = self.connectivity(self.plane if self.opts.slice else i)

                if(self.opts.save):
                    self.save


        elif self.opts.integrate:
            #Integration of parallel electric field along field lines
            self.init_integrate()
            for i in range(0, self.zlen):

                self.integrate_plane(self.plane if self.opts.slice else i)
                self.q[:,:,i] = self.arrays['i']

            if(self.opts.save):
                self.save
