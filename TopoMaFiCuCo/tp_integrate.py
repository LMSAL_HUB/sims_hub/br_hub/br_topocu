
import os
import sys
import math
import numpy as np

from .tp_data import getData
from .tp_data import getDatanopars
from .tp_cuda import cudaManipulator

from pycuda import gpuarray
from pycuda.compiler import SourceModule

class integrate(getData, cudaManipulator):

    def init_integrate(self):

        '''
        Initializes tracing kernel
        '''

        with open(os.environ['CUDA_LIB']+'TopoMaFiCuCo/integrate.cu') as kernelfile:
            src_module = kernelfile.read()

        self.mod = SourceModule(src_module)
        self.kernel = self.mod.get_function('integrate')

    def integrate_slice(self, lower, upper, i):

        '''
        Traces the field between lower and upper bounds
        If i != 0, initializes tracing at plane i
        '''

        self.constants = {

            'step'      : 0.0005,
            'mintheta'  : 0.0000005,
            'maxlength' : 100000,
            'upper'     : self.axes['z'][upper],
            'lower'     : self.axes['z'][lower],
            'offset'    : upper

        }

        self.textures = {

            'bxc'  : self.data['bxc'][:,:,upper:lower],
            'byc'  : self.data['byc'][:,:,upper:lower],
            'bzc'  : self.data['bzc'][:,:,upper:lower],
            'ix'  : self.data['ix'][:,:,upper:lower],
            'iy'  : self.data['iy'][:,:,upper:lower],
            'iz'  : self.data['iz'][:,:,upper:lower]

        }

        self.load_constants()
        self.load_axes()
        self.load_textures()

        d_arrays = {}
        d_arrays.update({a : gpuarray.to_gpu(self.arrays[a]) for a in self.arrays.keys()})

        self.kernel(np.intc(i), *d_arrays.values(), block = self.block, grid = self.grid)

        self.arrays.update({a : d_arrays[a].get() for a in d_arrays.keys()})



    def integrate_plane(self, plane):

        '''
        Traces field lines passing through [plane] until they return to it.
        Integrates parallel electric field
        '''

        #Initializes data arrays

        arrays = ['x', 'y', 'z', 'd', 'i']
        template = np.zeros((self.xl, self.yl), dtype = np.float32, order = 'F')

        self.arrays = {}
        self.arrays.update({a : np.zeros_like(template) for a in arrays})

        #Calculates domain splits

        #split_size = math.floor(256 * 256 * 128 / float(self.xl * 2 * self.yl))
        split_size = math.floor(256 / float(2))
        splits  = np.arange(0, plane, split_size)
        splits  = np.append(splits, plane)[::-1]

        #Traces

        for i in range(0, splits.size - 1):
            self.integrate_slice(splits[i], splits[i + 1], plane if i == 0 else 0)

        splits = splits[::-1]

        for i in range(1, splits.size - 1):
            self.integrate_slice(splits[i], splits[i + 1], 0)

    def __init__(self):
        super(integrate, self).__init__()


class integratenopars(getDatanopars, cudaManipulator):

    def init_integrate(self):

        '''
        Initializes tracing kernel
        '''

        with open(os.environ['CUDA_LIB']+'TopoMaFiCuCo/integrate.cu') as kernelfile:
            src_module = kernelfile.read()

        self.mod = SourceModule(src_module)
        self.kernel = self.mod.get_function('integrate')

    def integrate_slice(self, lower, upper, i):

        '''
        Traces the field between lower and upper bounds
        If i != 0, initializes tracing at plane i
        '''

        self.constants = {

            'step'      : 0.0005,
            'mintheta'  : 0.0000005,
            'maxlength' : 100000,
            'upper'     : self.axes['z'][upper],
            'lower'     : self.axes['z'][lower],
            'offset'    : upper

        }

        self.textures = {

            'bxc'  : self.data['bxc'][:,:,upper:lower],
            'byc'  : self.data['byc'][:,:,upper:lower],
            'bzc'  : self.data['bzc'][:,:,upper:lower],
            'ix'  : self.data['ix'][:,:,upper:lower],
            'iy'  : self.data['iy'][:,:,upper:lower],
            'iz'  : self.data['iz'][:,:,upper:lower]

        }

        self.load_constants()
        self.load_axes()
        self.load_textures()

        d_arrays = {}
        d_arrays.update({a : gpuarray.to_gpu(self.arrays[a]) for a in self.arrays.keys()})

        self.kernel(np.intc(i), *d_arrays.values(), block = self.block, grid = self.grid)

        self.arrays.update({a : d_arrays[a].get() for a in d_arrays.keys()})



    def integrate_plane(self, plane):

        '''
        Traces field lines passing through [plane] until they return to it.
        Integrates parallel electric field
        '''

        #Initializes data arrays

        arrays = ['x', 'y', 'z', 'd', 'i']
        template = np.zeros((self.xl, self.yl), dtype = np.float32, order = 'F')

        self.arrays = {}
        self.arrays.update({a : np.zeros_like(template) for a in arrays})

        #Calculates domain splits

        #split_size = math.floor(256 * 256 * 128 / float(self.xl * 2 * self.yl))
        split_size = math.floor(256 / float(2))
        splits  = np.arange(0, plane, split_size)
        splits  = np.append(splits, plane)[::-1]

        #Traces

        for i in range(0, splits.size - 1):
            self.integrate_slice(splits[i], splits[i + 1], plane if i == 0 else 0)

        splits = splits[::-1]

        for i in range(1, splits.size - 1):
            self.integrate_slice(splits[i], splits[i + 1], 0)

    def __init__(self,opts=None,parent=None):
        super(integratenopars, self).__init__(opts)
